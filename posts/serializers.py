from .models import Post, Proyects
from rest_framework import serializers

class PostSerializer(serializers.ModelSerializer):
  class Meta:
    model = Post
    fields = '__all__'

class ProyectsSerializer(serializers.ModelSerializer):
  class Meta:
    model = Proyects
    fields = '__all__'