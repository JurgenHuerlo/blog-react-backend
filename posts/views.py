from .models import Post, Proyects
from .serializers import PostSerializer, ProyectsSerializer
from rest_framework import viewsets

class PostViewSet(viewsets.ModelViewSet):
  queryset = Post.objects.all()
  serializer_class = PostSerializer

class ProyectsViewSet(viewsets.ModelViewSet):
  queryset = Proyects.objects.all()
  serializer_class = ProyectsSerializer