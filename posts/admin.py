from django.contrib import admin
from .models import Post, Proyects

# Register your models here.
class PostAdmin(admin.ModelAdmin):
    #para mostrar los datos de hora de creación y modificación
    readonly_fields =('created', 'updated')
    list_display = ('title','created',)
    list_filter = ('title',)
    search_fields = ('title','description')

admin.site.register(Post, PostAdmin)

class ProyectsAdmin(admin.ModelAdmin):
    #para mostrar los datos de hora de creación y modificación
    readonly_fields =('created', 'updated')
    list_display = ('title','created',)
    list_filter = ('title',)
    search_fields = ('title','description')

admin.site.register(Proyects, ProyectsAdmin)