from django.contrib import admin
from django.urls import path, include

from posts import views
from rest_framework import routers
from django.conf import settings

router = routers.DefaultRouter()

# En el router vamos añadiendo los endpoints a los viewsets
router.register('post', views.PostViewSet)
router.register('proyects',views.ProyectsViewSet)

urlpatterns = [
  path('api/v1/', include(router.urls)),
  path('admin/', admin.site.urls),
  path('api/v1/auth/', include('rest_auth.urls')),
  path('api/v1/auth/registration/', include('rest_auth.registration.urls')),
] 

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns+= static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)